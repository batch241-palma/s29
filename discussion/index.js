// QUERY OPERATORS

// $gt / $gte operator
/*
- Allows us to have documents that have field number values greater that or equal to a specified value.
- Syntax:
	db.collectionName.find({field: {$gt: value}})
	db.collectionName.find({field: {$gte: value}})
*/
db.users.find({age: {$gt:65}});
db.users.find({age: {$gte:65}});

// $lt // $lte operator
/*
- Allows us to find documents that have field number less than or equal to a specified value
- Syntax
	db.collectionName.find({field: {$lt: value}})
	db.collectionName.find({field: {$lte: value}})
*/
db.users.find({age: {$lt:76}});
db.users.find({age: {$lte:76}});

// $ne operator
/*
- Allows us to find documents that have a specified value that are not equal to a specified value.
- Syntax:
	db.collectionName.find({field: {$ne: value}})
*/
db.users.find({age: {$ne: 82}});

// LOGICAL QUERY OPERATORS

//  $or
/*
- Allows us to find documents that match a single criteria from multiple search criteria provided.
- Syntax:
	db.collectionName.find({$or: [{"fieldA: valueA"}, {"fieldB: valueB"}]})
*/
db.users.find({$or: [{firstName: "Neil"}, {firstName: "Bill"}]});
db.users.find({$or: [{firstName: "Neil"}, {firstName: "Bill"},{firstName: "Stephen"}]});
db.users.find({$or: [{firstName: "Neil"}, {firstName: "Bill"},{firstName: "Stephen"}, {firstName: "Jane"}]});
db.users.find({$or: [{firstName:"Neil"}, {age:{$gt: 30}}]});

// $and operator
/*
- Allows us to find documents matching multiple criteria in a single field.
- Syntax:
	db.collectionName.find({$and:
	[{fieldA:valuaA},{fieldB:valueB}]
	})
*/
db.users.find({$and:
	[{age:{$ne:82}}, {age:{$ne:76}}
	]});

// FIELD PROJECTION
/*
- When we retrieve documents, MongoDB usually returns the whole document.
- There are times when we only need specific fields.
- For those cases, we can include or exclude fields from the response.
*/

// INCLUSION
/*
- Allows us to include / add specific fields only when retrieving documents.
- We write 1 to include fields
-Syntax:
	db users.find({criteria}, {field:1})
*/
db.users.find(
	{
		firstName: "Jane"	
	},
	{
		firstName: 1,
		lastName: 1,
		"contact.phone": 1
	}
)

// EXCLUSION
/*
- Allows us to exclude / remove specific fields when displaying documents
- The value provided is "0" to denote that the field is being excluded.
- Syntax:
	db.users.find({criteria},{field:0})
*/
db.users.find(
	{
		firstName: "Jane"
	},
	{
		contact:0,
		department: 0
	}
);

// Supressing the ID field
/*
- Allows us to exclude the  "_id" field when retrieving documents.
- When using field projection, field inclusion and exclusion may not be used at the same time.
- The "_id" is exempted to this rule.
- Syntax:
	db.users.find({criteria},{_id:0})
*/
db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		contact: 1,
		_id: 0
	}
);

// EVALUATION OF QUERY OPERATORS

// $regex operator
/*
- Allows us to find documents that match a specific string patterng using regular expressions.
-Syntax:
	db.users.find({field: $regex: 'pattern', $options: '$optionsValue'})
*/

// Case Sensitive Query
db.users.find({firstName: {$regex: 'N'}});

// Case Insensitive Query
db.users.find({firstName: {$regex: 'N', $options: '$i'}});